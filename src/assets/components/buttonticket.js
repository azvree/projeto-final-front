import React from 'react';
import '../css/buttonticket.css';
import {Link} from 'react-router-dom'


const ButtonTick = (props) => {
    return(
        //<button className="button" onClick={props.buttonFunction}> {props.children}</button>
        <a className='button' href='https://www.mercadopago.com.br/checkout/v1/redirect?pref_id=402488059-3f5f6765-4aa7-427e-9887-c7574ca3a049'>{props.children}</a>
    );
};

export default ButtonTick;