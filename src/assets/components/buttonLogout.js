import React from 'react';
import '../css/buttonticket.css';
import {Link} from 'react-router-dom'


const ButtonLogout = (props) => {
    return(
        <Link to={props.path}><button className="button" onClick={props.buttonFunction}> {props.children} </button></Link>
    );
};

export default ButtonLogout;
