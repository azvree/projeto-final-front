import axios from 'axios';

const API = axios.create({
    baseURL: "https://intense-hollows-99753.herokuapp.com/",
    respondeType: "json"
});

export default API;