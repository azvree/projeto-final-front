import React from 'react';
import Locacao from '../assets/components/locacao';
import "../assets/css/gerenciador.css"
import LocacaoRemov from '../assets/components/locacaoremov';
import AddProduto from '../assets/components/adicionarprodutos';
import MaxDateForm from '../assets/components/datasmaxform';
import VoteDate from '../assets/components/votedate';


const Gerenciador = () => {
    return(
        <section className="gerenciadorBack">

            <div className="gerenciadorDiv">
                <Locacao/>
            
                <LocacaoRemov/>
            </div>

            <div className="gerenciadorDiv">
                <p>Datas</p>

                <MaxDateForm/>

                <VoteDate/>
            </div>
            
            <div className="gerenciadorDiv ">
                <AddProduto/>
            </div>

        </section>

    )
}

export default Gerenciador;