import React from 'react';
import {ReactComponent as NotfoundLogo} from '../assets/img/loboerror.svg'
import '../assets/css/notfound.css'



const Notfound = () => {
    return(
        <section className="error">

            <h1 className="fontSize">Página não encontrada! ERROR 404</h1>

            <NotfoundLogo className="imageError"/>
        </section>

    )
}

export default Notfound;