import React from 'react';
import Header from '../../assets/components/header';
import Footer from '../../assets/components/footer';
import logged_in from '../../assets/components/auth/loggedIn';

const Layout = (props) => {
    return(
        <React.Fragment>
            <Header navStatus={logged_in()}/>
                {props.children}
            <Footer/>
        </React.Fragment>
    );
};

export default Layout;