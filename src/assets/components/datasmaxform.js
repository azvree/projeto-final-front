import React, { Component } from "react";
import axios from "axios";
import "../css/dates.css"
import config from "./config/config";

export default class MaxDateForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
        maxDateForm:""
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);

  }

  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleSubmit(event) {
    const { maxDateForm } = this.state;
        

    console.log(this.state)
    axios
      .patch(
        "http://intense-hollows-99753.herokuapp.com/events/1",
        { 
          event: {
            maximum_date:maxDateForm
          }
        },config())
    event.preventDefault();
  }


  render() {
    return (
      <div className="maxDateBack">
        <form className="maxDateReg" onSubmit={this.handleSubmit}>
          
        <div className="maxDateAtr"> 
          <label className="maxDateLabel">Atualizar uma data máxima para entrega dos formulários</label>  
          <input
              type="date"
              name="maxDateForm"
              value={this.state.maxDateForm}
              onChange={this.handleChange}
              required
            />
        </div> 

          
        <div className="maxDateAtr"> 
          <button className="maxDateButton" type="submit">Atualizar data máxima</button>
        </div>  
        </form>
      </div>
    );
  }
}


