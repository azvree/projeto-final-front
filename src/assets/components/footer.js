import React from 'react';
import '../css/footer.css';


const Footer = () => {
    return(
        <footer className="footer">
            <div className="footerElem">
                <p>Desenvolvido por:</p>
            </div>
            <div className="footerElemDir">
                <p className="footerEmail">Email:</p>
                <p>Telefone:</p>
            </div>
        </footer>
    )
}

export default Footer;