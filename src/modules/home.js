import React, {useState} from 'react';
import ButtonTick from '../assets/components/buttonticket';
import '../assets/css/home.css';
import {ReactComponent as Mapa} from '../assets/img/image 2.svg'
import API from './api/api';
import loggedIn from '../assets/components/auth/loggedIn';
import config from '../assets/components/config/config';



class Home extends React.Component {
    state = {
        local:"",
        date:""
    }

    componentWillMount() {
        if (loggedIn()){
            API.get('/events', config()).then(resp => {
                this.setState({
                    local: resp['data'][0].location,
                    date: resp['data'][0].date_event
                })
            })
        }
        else{
            this.setState({local: "Entre para saber", date: "Entre para saber"})
        }
    }

    render(){
        return(
            <React.Fragment>
            <section className="sec1">
                <div className="content">
                    <h1 className="titulo_sec1">Bem-vindo ao churrasquIN</h1>
                </div>
                
                <div className="content2">
                    <p className="texto">Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                    Morbi odio enim, fringilla non justo sed, euismod porta velit.
                    Integer eu mauris eu turpis consectetur posuere. Integer bibendum mi sit amet leo elementum. 
                    Donec a elit nibh. Morbi venenatis hendrerit nulla, nec fermentum odio volutpat ut. 
                    In augue ipsum, pretium pulvinar tortor eu, luctus semper erat. 
                    Sed vel mi massa. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. 
                    Fusce auctor est a accumsan dignissim. Proin blandit convallis quam, vitae interdum ligula euismod nec. 
                    Suspendisse hendrerit est sit amet dapibus porttitor. 
                    Duis a erat eget diam auctor egestas sit amet sit amet nisi.</p>
                </div>

                <div className="btn">
                <ButtonTick path="/ingresso" className="btn-comprar">Comprar ingresso</ButtonTick>
                </div>
            </section>
            <section className="sec2">
                <div className="conteuto_sec2">
                    <div>
                        <h1 className="titulo_sec2">informações</h1>
                    </div>
                    <div className="segundo-conteudo"> 
                        <div className="content_info">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi odio enim, fringilla non justo sed, euismod porta velit. Integer eu mauris eu turpis consectetur</p> 
                        </div>       
                        <div className="linha-vertical"></div>   
                        <div className="local-info">
                            <p className="destaque">Local:</p>
                            <p className="endereco">{this.state.local}</p>
                            <p className="destaque">Hora:</p>
                            <p className="endereco">{this.state.date}</p>
                            
                        </div>
                        <Mapa className="mapa"/>
                    </div>                 
                </div>
            </section>
        </React.Fragment>
    )
}
}

export default Home;