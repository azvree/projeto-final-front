import React from 'react';
import {Button, Modal} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../css/btnAdmin.css'

const ModalButton = (props, setState) => {
    const [show, setShow] = React.useState(false);
    

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
  
    return (
      <React.Fragment>
        <Button className="btnAdmin" variant="primary" onClick={handleShow}>
          {props.primarytext}
        </Button>
  
        <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>{props.headerModal}</Modal.Header>
        <Modal.Body>{props.bodycontenttext}
        {props.bodycontentvar}
        </Modal.Body>
        </Modal>

      </React.Fragment>
    );
  }

export default ModalButton;