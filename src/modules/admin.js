import React, { Component } from 'react';
import ModalButton from '../assets/components/modalbutton';
import BtnAdmin from '../assets/components/botaoAdmin';
import '../assets/css/admin.css';
import '../assets/css/btn-admin.css';
import API from './api/api';
import config from '../assets/components/config/config';


class AdminPage extends Component {

    state = {
        ticket:"",
        fund:"",
        cost:"",
        products:[]
    }
    componentWillMount(){
        API.get('events', config())
        .then(
            resp => {
                this.setState({
                fund: resp['data'][0].fund,
                cost: resp['data'][0].estimated_cost,
                ticket: resp['data'][0].ticket
                })
            }
        )

        API.get('products', config())
        .then(
            resp => {
              console.log(resp['data'])
              this.setState({
                  
              })
                }
        
        )
    }   


    render() {
    return(
        <React.Fragment>
            <section className="sec-admin">
            <div className="titulo-admin">
                <p className="title-pg">gerenciador do evento</p>
            </div>

            <div className="botoes-admin">

                <BtnAdmin path="/gerenciador"><p className="bt-admin">Gerenciar</p></BtnAdmin>

                <BtnAdmin path="/viewform"><p className="bt-admin">Resposta dos Formulários</p></BtnAdmin>

                

                <ModalButton
                className="botao-admin"
                primarytext="Total arrecadado"
                bodycontentvar2={this.state.fund}
                bodycontenttext= "Total arrecado R$: "
                bodycontentvar={this.state.fund}
                />

                <ModalButton
                className="botao-admin"
                primarytext="Gastos Previstos"
                bodycontenttext="Gastos R$: "
                bodycontentvar={this.state.cost}/>


                <ModalButton
                className="botao-admin"
                primarytext="Produtos"
                headerModal="Lista de produtos"/>

            </div>
        </section>
        </React.Fragment>
    );

}

}

export default AdminPage;