import React from 'react'
import '../assets/css/viewForm.css'
import config from '../assets/components/config/config';
import API from './api/api';

const ListItem = ({ value, onClick }) => (
    <li className="liImutavel" onClick={onClick}>{value}</li>
  );
  
const List = ({ items, onItemClick }) => (
    <ul className="ulImutavel">
      {
        items.map((item, i) => <ListItem key={i} value={item} onClick={onItemClick} />)
      }
    </ul>
  );
  
export default class ViewForm extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        inputValue: '',
        fruites: []
      };
    }
  
    componentWillMount = () => {
        const usersnames = []
        API.get('/users', config()).then(users => {
          users['data'].forEach(user => {
            usersnames.push([user.id,  user.name])
          })
        })
        
        API.get('/forms', config())
        .then(resp => {
            const pop = []
            
            resp['data'].forEach (element => {
              
              let name = usersnames.filter((value)=>(value[0]===element.user_id))[0][1]
              
              pop.push("O usuário " + name + " (" + element.user_id + ") prefere a data " + element.chosen_date + ". Ele disse que sua restrição à comida é " + element.food_restriction + ", sua restrição à bebida é " + element.drink_restriction + " e tem alergia à " + element.allergy + ". Ele também acrescentou o seguinte comentário: " + element.comment + ".")
                // const nextState = [...fruites, inputValue];
                
            });
            this.setState({ fruites:pop });
        }) 
        
    }
    
  
    // onChange = (e) => this.setState({ inputValue: e.target.value });
  
    
  
    render() {
      const { fruites} = this.state;
      return (
          <div>          
            <List items={fruites} onItemClick={this.handleItemClick} />
          </div>
      );
    }
  }
  