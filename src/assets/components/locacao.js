import React, { Component } from "react";
import axios from "axios";
import "../css/locacao.css"
import config from "./config/config";

export default class Locacao extends Component {
  constructor(props) {
    super(props);

    this.state = {
        addLocation:""
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);

  }

  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleSubmit(event) {
    const { addLocation } = this.state;
        

    axios
      .patch(
        "http://intense-hollows-99753.herokuapp.com/events/1",
        { 
          event: {
            location: addLocation
          }
        }, config())
    event.preventDefault();
  }


  render() {
    return (
      <div className="locationBack">
        <form className="locationReg" onSubmit={this.handleSubmit}>
          
        <div className="locationAtr"> 
          <label className="locationLabel">Locação</label>  
          <input
              type="text"
              name="addLocation"
              placeholder="Endereço completo"
              value={this.state.addLocation}
              onChange={this.handleChange}
              required
            />
        </div> 

          
        <div className="locationAtr"> 
          <button className="locationButton" type="submit">Adicionar locação</button>
        </div>  
        </form>
      </div>
    );
  }
}


