import React from 'react';
import '../css/header.css';
import {ReactComponent as LoboLogo} from '../img/logochurrasquin.svg'
import ButtonTick from '../components/buttonticket'
import ButtonLogin from './buttonlogin';
import ButtonLogout from './buttonLogout'
import MenuHamburg from './menu';
import { Link } from 'react-router-dom';
import logout from './auth/logout'
import loggedIn from './auth/loggedIn';

const Header = (props) => {
    if(loggedIn()){
        return(
            <React.Fragment>
            <header className="display spaceBetween">
                    <MenuHamburg />
    
                    <Link className="loboLogo" to="/"><LoboLogo/></Link>
                    
                    <nav className ="display center navega">
                        <ul className="display ">
                        
                            <li><ButtonTick exact path="/ingresso" >Comprar ingresso</ButtonTick></li>
                        
                            <li><ButtonLogout exact path='/' buttonFunction={logout}>Sair</ButtonLogout></li>
    
                            
                        
                        </ul>
                    </nav>
            
    
            </header>
            </React.Fragment>
        )
    }
    else{

        return(
        <React.Fragment>
        <header className="display spaceBetween">
                <MenuHamburg />

                <Link className="loboLogo" to="/"><LoboLogo/></Link>
                
                <nav className ="display center navega">
                    <ul className="display ">
                    
                        <li><ButtonTick exact path="/ingresso" >Comprar ingresso</ButtonTick></li>
                    
                        <li><ButtonLogin exact path='/login'>Login</ButtonLogin></li>

                        <li><ButtonLogin exact path='/registro'>Registrar-se</ButtonLogin></li>
                    
                    </ul>
                </nav>
        

        </header>
        </React.Fragment>
    )}
}

export default Header;