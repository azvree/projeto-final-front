import React from 'react';
import { slide as Menu } from 'react-burger-menu';
import { NavLink} from 'react-router-dom';
import '../css/menu.css'
import logged_in from "./auth/loggedIn";
import is_admin from "./auth/isAdmin"


const MenuHamburg = () =>  {
     

    if (!logged_in()){
        return(
            <React.Fragment>
                <Menu>
                    <NavLink className="menu-item" exact to="/" activeClassName="current" >Home</NavLink>
                    <NavLink className="menu-item" exact to="/login" activeClassName="current" >Login</NavLink>
                    <NavLink className="menu-item" exact to="/registro" activeClassName="current" >Registre-se</NavLink>
                </Menu>
            </React.Fragment>        
            )
    }
    else{

        if (is_admin()){
            return(
            <React.Fragment>
            <Menu>
                <NavLink className="menu-item" exact to="/" activeClassName="current" >Home</NavLink>
                <a className = "menu-item" href="https://www.mercadopago.com.br/checkout/v1/redirect?pref_id=402488059-3f5f6765-4aa7-427e-9887-c7574ca3a049">Comprar Ingresso</a>
                <NavLink className="menu-item" exact to="/formulario" activeClassName="current">Formulario</NavLink>
                <NavLink className="menu-item" exact to="/admin" activeClassName="current">Pagina Admin</NavLink>
            </Menu>
            </React.Fragment>
            )
        }
        else{
            return(
            <React.Fragment>
            <Menu>
                <NavLink className="menu-item" exact to="/" activeClassName="current" >Home</NavLink>
                <a className = "menu-item" href="https://www.mercadopago.com.br/checkout/v1/redirect?pref_id=402488059-3f5f6765-4aa7-427e-9887-c7574ca3a049">Comprar Ingresso</a>
                <NavLink className="menu-item" exact to="/formulario" activeClassName="current">Formulario</NavLink>
                
            </Menu>
            </React.Fragment>
            )
        }
    
    
    }
}
    
export default MenuHamburg;