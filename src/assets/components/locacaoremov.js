import React, { Component } from "react";
import "../css/locacao.css"
import config from "./config/config";
import API from "../../modules/api/api";

export default class LocacaoRemov extends Component {
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);

  }


  handleSubmit(event) {

    API.patch("/events/1",
        { 
          event: {
            location: "Sem local"
          }
        }, config())
    event.preventDefault();
  }


  render() {
    return (
      <div className="locationRemovBack">
        <form onSubmit={this.handleSubmit}>
          <button className="locationRemovButton" type="submit" >Remover locação</button>
          
        </form>
      </div>
    );
  }
}


