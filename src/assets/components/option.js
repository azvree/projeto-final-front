import React, {useState} from 'react'
import config from './config/config';
import API from '../../modules/api/api';
import dataStorage from './dataStorage';


const ListItem = ({ value, onClick }) => (
    <li onClick={onClick}>{value}</li>
  );
  
const List = ({ items, onItemClick }) => (
    <ul>
      {
        items.map((item, i) => <ListItem key={i} value={item} onClick={onItemClick} />)
      }
    </ul>
  );

  
export default class Options extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        inputValue: '',
        fruites: []
      };
    }
    // componentWillMount(){
    //   dataStorage("start")
    // }
    onClick = () => {
        
        
        API.get('https://intense-hollows-99753.herokuapp.com/votations', config())
        .then(resp => {
            
            const pop = []
            resp['data'].forEach (element => {
              pop.push(element.date_possible)
                // const nextState = [...fruites, inputValue];
                
            });
            this.setState({ fruites:pop });
        }) 
        
    }
    
  
    // onChange = (e) => this.setState({ inputValue: e.target.value });
  
    handleItemClick = (e) => {
      alert(e.target.value)    
      
    }
    
  
    render() {
      const { fruites} = this.state;
      return (
        <div className = 'sec_viewForm'>
          <button className="buttonDates" onClick={this.onClick}>Me mostre as opções</button>

          <List items={fruites} onItemClick={this.handleItemClick} />
          

        </div>
      );
    }
  }
  