import React from "react";
import './App.css';
import Layout from '../hoc/layout/Layout';
import Home from '../modules/home'
import {BrowserRouter, Switch, Route, Redirect} from 'react-router-dom'
import createBrowserHistory from 'history/createBrowserHistory';
import Notfound from '../modules/notfound';
import Register from '../modules/register';
import LoginPage from '../modules/login'
import Gerenciador from "../modules/gerenciador";
import ViewsForms from '../modules/viewForm'
import Formulario from '../modules/formulario'
import AdminPage from "../modules/admin";
import logged_in from '../assets/components/auth/loggedIn'
import is_admin from '../assets/components/auth/isAdmin'
const history = createBrowserHistory();





function App() {

  return (
    <BrowserRouter history={history}>
    <Layout>
        <Switch>
          <Route exact path="/" ><Home/></Route>
          <Route exact path="/admin" >{is_admin() ? <AdminPage/> : <Notfound/>}</Route>
          <Route exact path="/viewform">{is_admin()? <ViewsForms/> : <Notfound/>}</Route>
          <Route exact path='/ingresso'><p>pagamento</p></Route>
          <Route exact path='/login'>{logged_in()?  <Redirect to="/"/> : <LoginPage/>}</Route>
          <Route exact path='/formulario'>{logged_in()? <Formulario /> : <LoginPage/>}</Route>
          <Route exact path="/registro" ><Register/></Route>
          <Route exact path="/gerenciador">{is_admin()? <Gerenciador/> : <Notfound/>}</Route>

          <Route exact path="/recuperarsenha" ><p>Recuperar senha</p></Route>
          <Route exact path="*" ><Notfound/></Route> 
        </Switch>
    </Layout>
    </BrowserRouter>
  );
}

export default App;