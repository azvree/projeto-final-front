import React, { Component } from "react";
import "../../css/registration.css"
import { Redirect} from "react-router-dom";
import API from "../../../modules/api/api";

export default class Registration extends Component {
  constructor(props) {
    super(props);

    this.state = {
      nome:"",
      email: "",
      password: "",
      password_confirmation: "",
      kind:"",
      registrationErrors: "",
      redirect:false
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleSubmit(event) {
    const { nome, email, password, password_confirmation} = this.state;
    

    if (password !== password_confirmation){
      alert("A senha e a confirmação da senha precisam ser iguais."); event.preventDefault()
      ;}

      else{
        if ( this.state.kind === ""){
          this.setState({kind: "member"}); 
        }
    API.post(
        "/signup",
        { 
          user: {
            name: nome,
            email: email,
            password: password,
            kind:this.state.kind
          }
        },
    
      )
      .then(() => alert("Criado com sucesso!!"))
      .then(() => this.homeCall())     
      .catch(() => alert("Error ao criar a conta, tente novamente"));
    event.preventDefault();
    
  }}
  homeCall = () => {
    this.setState({
      redirect: true
    })
   }

  render() {
    if(this.state.redirect) {
      return <Redirect to="/" />
    }
    else{

      return (
        <div className="formBack">
        <form className="formReg" onSubmit={this.handleSubmit}>
          
        <div className="formAtr"> 
          <label>Nome</label>  
          <input
              type="text"
              name="nome"
              placeholder="Nome completo"
              value={this.state.nome}
              onChange={this.handleChange}
              required
              />
        </div> 

        <div className="formAtr"> 
          <label>Email</label>
          <input
            type="email"
            name="email"
            placeholder="Email"
            value={this.state.email}
            onChange={this.handleChange}
            required
            />
        </div>

        <div className="formAtr"> 
          <label>Senha</label>
          <input
            type="password"
            name="password"
            placeholder="Senha"
            value={this.state.password}
            onChange={this.handleChange}
            required
          />
        </div>
      
        <div className="formAtr"> 
          <label>Confirme sua senha</label>
          <input
            type="password"
            name="password_confirmation"
            placeholder="Confirme sua senha"
            value={this.state.password_confirmation}
            onChange={this.handleChange}
            required
            />
        </div>

        <div className="formAtr"> 
          <label>Eu sou:</label>
           <select name="kind" value={this.state.kind} onChange={this.handleChange}>

            <option 
            name="member"
            value="member"
            > Membro </option>

            <option
            name="ex_member"
            value="ex_member"
            > Ex membro</option>
           </select>
        </div>   

        <div className="formAtr"> 
           <button className="formButton" type="submit">Enviar</button>
        </div>  
        </form>
      </div>
    );
  }
  }
}
