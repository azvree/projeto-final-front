import React from 'react';
import '../css/buttonAdmin.css';
import {Link} from 'react-router-dom'


const ButtonAdmin = (props) => {
    return(
        <Link to={props.path}><button className="buttonAdmin" onClick={props.buttonFunction}> {props.children} </button></Link>
    );
};

export default ButtonAdmin;