import React, { Component} from 'react';
import '../assets/css/login.css';
import API from './api/api';
import { Link } from 'react-router-dom';

class LoginPage extends Component { 

    state = {
        user:"",
        password:"",
        redirect:false
    }
    
    render () {
        const { password, user} = this.state
       
        

            return (
                <section className="sec-login">
                <form className="form-login" onSubmit ={this.handleSubmit}>
                <h1 className="titulo-pagina">Login</h1>
                <div className="bg">
                    <div className="bg-content">
                        <div className="item">
                            <label className="titulo-login">Email</label>
                            <input className="input" 
                            onChange={this.handleUserChange}
                            name="user" 
                            placeholder="email"
                            type="text"
                            value={user} />
                        </div>

                        <div className="item">
                            <label className="titulo-login">Senha</label>
                            <input className="input" 
                            name="password" 
                            onChange={this.handlePasswordChange}
                            placeholder="Digite sua senha" 
                            type="password" 
                            value={password} />
                        </div>
                        <div className="botao">
                        <button className="login-btn" type='submit'>Entrar</button>
                        </div>
                        <Link to="/recuperarsenha" className="link">Recuperar senha</Link>
                        <Link to="/registro" className="link">Cadastre-se</Link>
                    </div>
                    
                </div>
                
            </form>
            </section>
            )
        
    }
    
    

    handleUserChange = event => this.setState({user: event.target.value})
    handlePasswordChange = event => this.setState({password: event.target.value})

    handleSubmit = event => {
        const {password, user} = this.state
        
        event.preventDefault()
        

        API.post('login',{"user": {
            "email":user,
            "password" :password
            }})
        .then(resp => {
            this.setState({userToken:resp.headers['authorization']})
            sessionStorage.setItem('userToken', resp.headers['authorization'])
            
        }).then(() => {window.location.reload()})
        .catch (error => alert(error) )
    }
}

export default LoginPage;