import React from 'react';
import '../assets/css/register.css'
import Registration from '../assets/components/auth/Registration';


const Register = () => {
    return(
        <section className="registerSection">
            <h1 className="register">Registro</h1>

            <Registration/>

        </section>
    )
}

export default Register;