import React, { useState } from 'react'
import API from './api/api'
import '../assets/css/form.css'
import Calendar from 'react-calendar'
import { Formik } from "formik";
import Options from '../assets/components/option';

import config from '../assets/components/config/config';

const Formulario = () =>{
    const [date, setdate] = useState("")
    
  

        return (
            <React.Fragment>
        
        <section className="sec-form">
        <h1 className="">Formulario</h1>

        
        <div className="formContainer">
        <Options/>
          
          <div>
              
              
              <Formik initialValues={{ chosen_date: '', food_restriction: 'no_food_restriction', drink_restriction: 'no_drink_restriction', allergy: '', comment: ''}}
                onSubmit={(values, { setSubmitting }) => {
                    setTimeout(() => {
                        API.post("/forms",{"form": values}, config()).then(()=>{alert(values)}).catch(alert("não foi"))
                        setSubmitting(false);
                    }, 400);
                }}>

                {({
                    values,
                    errors,
                    touched,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    isSubmitting,
                    /* and other goodies */
                }) => (
                    <form onSubmit={handleSubmit} className="form">
                    <div>
                        <label> Escolha uma data de sua preferencia para o churrasco </label>
                        
                        <input
                            name="chosen_date"
                            value={values.date}
                            onChange={handleChange}
                            placeholder="Digite uma das datas possíveis">

                        </input>
                        

                    </div>
                        
                    <div>
                        <label>Selecione sua restrição alimentar</label>
                        <select
                            type="text"
                            name="food_restriction"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.food_restriction}
                            >
                            <option value = "no_food_restriction">Sem Restrição</option>
                            <option value = "vegetarian">Vegetariano</option>
                            <option value = "vegan">Vegano</option>
                        </select>
                    </div>
                    <div>
                        <label>Selecione sua restrição à bebidas alcoólicas</label>
                        <select
                            type="text"
                            name="drink_restriction"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.drink_restriction}
                            >
                            <option value = "no_drink_restriction">Sem Restrição</option>
                            <option value = "non_alcoholic">Nâo alcoólico</option>
                            
                        </select>
                    </div>
                    <div>
                        <label>Alguma alergia?</label>
                        <textarea
                            type="textarea"
                            placeholder="Digite suas alergias"
                            name="allergy"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.allergy}
                            />
                    </div>
                    <div>
                        <label>Gostaria de acrescentar algo? Adicione um comentario.</label>
                        <textarea
                            type="textarea"
                            name="comment"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.comment}
                            />
                    </div>
                    <button type="submit" disabled={isSubmitting}>
                        Submit
                    </button>
                                
                    </form>
                )}
            </Formik>

          </div>
        </div>
        </section>  
          
        
        </React.Fragment>
        ); 
    }
    
    
    export default Formulario