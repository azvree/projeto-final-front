import React, { Component } from "react";
import axios from "axios";
import "../css/dates.css"
import config from "./config/config";

export default class VoteDate extends Component {
  constructor(props) {
    super(props);

    this.state = {
        votForm:""
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);

  }

  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleSubmit(event) {
    const { votForm } = this.state;
        

    console.log(this.state)
    axios
      .post(
        "http://intense-hollows-99753.herokuapp.com/votations",
        { 
          votation: {
            date_possible: votForm
          }
        },config())
    event.preventDefault();
  }


  render() {
    return (
      <div className="voteFormBack">
        <form className="voteFormReg" onSubmit={this.handleSubmit}>
          
        <div className="voteFormAtr"> 
          <label className="voteFormLabel">Adicione uma data para votação dos formulários</label>  
          <input
              type="date"
              name="votForm"
              value={this.state.votForm}
              onChange={this.handleChange}
            
              required
            />
        </div> 

          
        <div className="voteFormAtr"> 
          <button className="voteFormButton" type="submit">Adicionar data possível</button>
        </div>  
        </form>
      </div>
    );
  }
}


