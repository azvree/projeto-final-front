import React from 'react';
import '../css/buttonlogin.css';
import {Link} from 'react-router-dom'


const ButtonLogin = (props) => {
    return(
        <Link to={props.path}>
            <button className="buttonlogin" onClick={props.buttonFunction}> {props.children} </button>
        </Link>

    )
}

export default ButtonLogin;