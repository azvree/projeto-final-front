import React, { Component } from "react";
import axios from "axios";
import "../css/addproduto.css"
import config from "./config/config";

export default class AddProduto extends Component {
  constructor(props) {
    super(props);

    this.state = {
      nome:"",
      price: "",
      amount_per_person: "",
      category: '2'
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleSubmit(event) {
    const { nome, price, amount_per_person,category } = this.state;
    

    console.log(this.state)
    axios
      .post(
        "http://intense-hollows-99753.herokuapp.com/products",
        { 
          product: {
            name: nome,
            price: price,
            amount_per_person: amount_per_person,
            category:category
          }
        },
    
        config())
;
    event.preventDefault();
  }


  render() {
    return (
      <div className="produtoBack">

          
        <form className="produtoReg" onSubmit={this.handleSubmit}>
          
        <div className="produtoAtr"> 
          <label>Nome do produto</label>  
          <input
              type="text"
              name="nome"
              placeholder="Nome do produto"
              value={this.state.nome}
              onChange={this.handleChange}
              required
            />
        </div> 

        <div className="produtoAtr"> 
          <label>Preço</label>
          <input
            type="number"
            name="price"
            step="0.1"
            placeholder="Preço do produto"
            value={this.state.price}
            onChange={this.handleChange}
            required
          />
        </div>

        <div className="produtoAtr"> 
          <label>Quantidade por pessoa em KG ou Litro</label>
          <input
            type="number"
            name="amount_per_person"
            value={this.state.amount_per_person}
            onChange={this.handleChange}
            required
          />
        </div>

        <div className="produtoAtr"> 
          <label>Categoria:</label>
           <select name="category" value={this.state.category} onChange={this.handleChange}>

            <option 
            name="drink"
            value="drink"
            > Bebida </option>

            <option
            name="food"
            value="food"
            > Comida</option>

           <option
            name="material"
            value="material"
            > Material</option>

           </select>
        </div>   

        <div className="produtoAtr"> 
          <button className="formButton" type="submit">Enviar</button>
        </div>  
        </form>
      </div>
    );
  }}

