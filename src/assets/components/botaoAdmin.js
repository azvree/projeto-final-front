import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../css/btnAdmin.css';
import {Link} from 'react-router-dom'


const BtnAdmin = (props) => {
    return(
        <Link to={props.path}>
            <button className="btnAdmin" onClick={props.buttonFunction}> {props.children} </button>
        </Link>

    )
}

export default BtnAdmin;